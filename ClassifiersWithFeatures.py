# # Proyecto 2 - Clasifiación multiclase de señales de sueño 

# ## Juan José García Cárdenas - Código: 201515655 y Adelaida Zuluaga - Código: 201424638

# ### Punto 1: Clasificación multiclase de señales de sueño - Clasificadores con Features extracted

# #### Importación de paquetes
import pyedflib as pyedf
import numpy as np
import matplotlib.pyplot as ptl
import os
import pandas as pd
import glob
import csv
import gc
import warnings
import math
import random
import tensorflow as tf
from IPython.display import clear_output
from sklearn.datasets import make_classification
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import train_test_split
from sklearn.neighbors import KNeighborsClassifier
from sklearn.multiclass import OneVsRestClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn import tree
from sklearn.svm import SVC
from collections import Counter
from imblearn.over_sampling import RandomOverSampler
from imblearn.under_sampling import RandomUnderSampler
from scipy import signal
from pathlib import Path
from tensorflow import keras
from tensorflow import metrics
from numpy.fft import *


class ClassifiersWithFeatures:
	
	def __init__(self):
		self.X_data_train = np.zeros([1,10])
		self.Y_data_train = []
		self.X_data_val = np.zeros([1,10])
		self.Y_data_val = []
		self.TRAIN_FILES = ['Features_train_data_W.csv','Features_train_data_L.csv','Features_train_data_P.csv','Features_train_data_REM.csv']
		self.VAL_FILES = ['Features_val_data_W.csv','Features_val_data_L.csv','Features_val_data_P.csv','Features_val_data_REM.csv']
		self.scalar = StandardScaler()

	def main(self):
		# Importacion datos con caracteristicas
		self.import_data()
		# Random Forest
		#self.rand_f()
		# SVM - One vs All
		self.svm_oneVsAll()
		# Decision tree
		#self.decision_tree()
		# KNN - K vecinos mas cercanos
		#self.k_n_n()
		# Red neuronal
		#self.neural_network_1_layer()
		#self.neural_network_multiple_layers()

	def rand_f(self):
		print("Random Forest")
		n_estimators=[100,150,200,250,300]
		bestEstimator = 100
		bestAccuracy = 0.5
		accuracyRandomForest = []
		for n in n_estimators:
			clfRF = RandomForestClassifier(n_estimators=n)
			print('Begin Training')
			clfRF.fit(self.X_data_train, self.Y_data_train)
			print('Begin Validation')
			accuracy = clfRF.score(self.X_data_val, self.Y_data_val)
			accuracyRandomForest.append(accuracy)
			print("Accuracy: ", clfRF.score(self.X_data_val, self.Y_data_val))
			if accuracy > bestAccuracy:
				bestAccuracy = accuracy
				bestEstimator = n
		print('Resultado: '+str(accuracy))
		print("N-Estimadores:")
		print("Mayor accuracy presentada: "+str(bestAccuracy))
		print("Número de estimadores de mayor accuracy: " + str(bestEstimator))

		ptl.plot(n_estimators,accuracyRandomForest)
		ptl.title('Precision de validacion')
		ptl.ylabel('Precision de validacion - Porcentaje')
		ptl.xlabel('Numero de estimadores')
		ptl.show()

		
	def import_data(self):
		for file in self.TRAIN_FILES:
			file_readed = pd.read_csv(file)
			file_readed = file_readed.to_numpy()
			x_data = np.zeros((len(file_readed),len(file_readed[0])-1))
			self.labels = []
			for i in range(0,len(file_readed)):
				self.labels.append(file_readed[i][-1])
				x_data[i] = file_readed[i][:-1]
			print("File imported :"+file)
			if len(self.X_data_train)==1:
				self.X_data_train = x_data
			else:
				self.X_data_train = np.concatenate((self.X_data_train,x_data),axis=0)
			self.Y_data_train = self.Y_data_train+self.labels
		self.Y_data_train = np.array(self.Y_data_train)
		print("Train Data imported")
		#self.X_data_train = self.scalar.fit_transform(self.X_data_train,self.Y_data_train)
		print("Train Data normalized")

		for file in self.VAL_FILES:
			file_readed = pd.read_csv(file)
			file_readed = file_readed.to_numpy()
			x_data = np.zeros((len(file_readed),len(file_readed[0])-1))
			self.labels = []
			for i in range(0,len(file_readed)):
				self.labels.append(file_readed[i][-1])
				x_data[i] = file_readed[i][:-1]
			print("File imported :"+file)
			if len(self.X_data_val)==1:
				self.X_data_val = x_data
			else:
				self.X_data_val = np.concatenate((self.X_data_val,x_data),axis=0)
			self.Y_data_val = self.Y_data_val+self.labels
		self.Y_data_val = np.array(self.Y_data_val)
		print("Validation Data imported")
		#self.X_data_val = self.scalar.fit_transform(self.X_data_val,self.Y_data_val)
		print("Validation Data normalized")


	def svm_oneVsAll(self):
		# Classifier
		print("SVM One Vs All")
		C = 10
		accuracy = []
		gam ='scale'
		bestC = 0
		bestGam = 0
		bestScore = 0.5
		clf = OneVsRestClassifier(SVC(kernel = 'rbf' , C= int(C), gamma = gam, probability = True))
		print('Training model')
		clf = clf.fit(self.X_data_train, self.Y_data_train)
		print('Validating model')
		y_prediction = clf.predict(self.X_data_val)
		result = clf.score(self.X_data_val, self.Y_data_val)
		accuracy.append(result)
		if result > bestScore:
			bestScore = result
		print("Accuracy: ",result)
		print("La precision es: "+bestScore)

	def decision_tree(self):
		print("Decision Trees")
		DT = tree.DecisionTreeClassifier()
		print('Begin Training')
		DT.fit(self.X_data_train, self.Y_data_train)
		Y_pred  = DT.predict(self.X_data_val)
		print("Accuracy: " +str(DT.score(self.X_data_val, self.Y_data_val)))

	def k_n_n(self):
		print("K Nereast Neighbors")
		n = [2,3,4,5,6,7,8,9]
		# Se crea los vectores que graficarán el error y exactitud en los datos de validación
		accuracyNeighbors = []
		# Se crean las variables de comparación
		bestAccuracy = 0.5
		bestNeighbor = 1
		# Se hace un recorrido simple para conocer el mejor número de vecinos para el clasificador
		for vecinos in n:
			neigh = KNeighborsClassifier(n_neighbors=vecinos,weights='distance',leaf_size=1000)
			print('Begin Training')
			neigh.fit(self.X_data_train, self.Y_data_train)
			print('Begin Validation')
			accuracy = neigh.score(self.X_data_val, self.Y_data_val)
			accuracyNeighbors.append(accuracy)
			if accuracy > bestAccuracy:
				bestAccuracy = accuracy
				bestNeighbor = vecinos
			print('Resultado: '+str(accuracy))
		print("K-Vecinos:")
		print("Mayor accuracy presentada: "+str(bestAccuracy))
		print("Número de vecinos de menor error: " + str(bestNeighbor))

		ptl.plot(n,accuracyNeighbors)
		ptl.title('Precision de validacion')
		ptl.ylabel('Precision de validacion - Porcentaje')
		ptl.xlabel('Numero de vecinos')
		ptl.show()

	def neural_network_1_layer(self):
		Nmax = 1000
		sampleNum = 30
		N = np.round(np.linspace(2,Nmax,sampleNum))
		# Four neurons exit
		Y_train_NN = np.matrix((len(self.Y_data_train),4))
		for i in range(0,len(self.Y_data_train)):
			Y_train_NN[i][int(self.Y_data_train[i])] = 1

		Y_val_NN = np.matrix((len(self.Y_data_val),4))
		for i in range(0,len(self.Y_data_val)):
			Y_val_NN[i][int(self.Y_data_val[i])] = 1
		# Número de epocas
		NEpochs = 10
		# Se crea el vector que contiene las funciones de activación a utilizar:
		# Lineal, Sigmoidal, Tangente hiperbolica y Relu
		funciones = ['linear', 'sigmoid', 'tanh', 'relu']
		# Se crea el vector que contiene los algoritmos de entrenamiento a iterar
		# SGD,  Adam, Adagrad
		algorit = ['sgd', 'adam', 'adagrad']
		# Se crea el vector que contiene las diferentes tasas de aprendizaje para los algoritmos de entrenamiento
		# 0.001, 0.01 y 0.1
		rates = [0.001, 0.01, 0.1]
		# Se crean los vectores de error y exactitud para definir la tasa de aprendizaje
		accuracy1 = []
		loss1 = []
		accuracy2 = []
		loss2 = []
		accuracy3 = []
		loss3 = []
		# Se crean los vectores de error y exactitud para definir la función de activación
		accuracyLinear = []
		lossLinear = []
		accuracyRelu = []
		lossRelu = []
		accuracyTanh = []
		lossTanh = []
		accuracySigmoid = []
		lossSigmoid = []
		accuracyExpo = []
		lossExpo = []
		# Se crean los vectores de error y exactitud para definir el algoritmo de entrenamiento
		accuracySGD = []
		lossSGD = []
		accuracyAdam = []
		lossAdam = []
		accuracyAdagrad = []
		lossAdagrad = []
		bestError = 0.5
		bestAccuracy = 0.5
		bestN = 2
		bestAlgorithm = ''
		bestiFuntion = ''
		bestRate = 0
		# Matriz de error
		ZerrorVal = np.zeros((len(N), len(funciones), len(algorit), len(rates)))
		# Matriz de accuracy
		ZaccuracyVal = np.zeros((len(N), len(funciones), len(algorit), len(rates)))
		for indexN, n in enumerate(N):
			for indexFunction, function in enumerate(funciones):
				for indexAlgoritmo, algoritmo in enumerate(algorit):
					for indexRate, tasa in enumerate(rates):
						if algoritmo == 'adagrad':
							opt = keras.optimizers.Adagrad(tasa)
						elif algoritmo == 'sgd':
							opt = keras.optimizers.SGD(tasa)
						else:
							opt = keras.optimizers.Adam(tasa)
						model = keras.Sequential([
							keras.layers.Dense(10, input_dim = 10),
							keras.layers.Dense(n, activation=str(function)),
							keras.layers.Dense(4, activation="sigmoid")
						])
						model.compile(loss= 'mean_squared_error', optimizer=opt, metrics=['accuracy'])
						model.fit(self.X_data_train, Y_train_NN, batch_size = 10,epochs=NEpochs, verbose=0)
						loss, accuracy = model.evaluate(self.X_data_val, Y_val_NN)
						ZerrorVal[indexN][indexFunction][indexAlgoritmo][indexRate] = loss
						ZaccuracyVal[indexN][indexFunction][indexAlgoritmo][indexRate] = accuracy
						if ZerrorVal[indexN][indexFunction][indexAlgoritmo][indexRate] < bestError:
							bestError = ZerrorVal[indexN][indexFunction][indexAlgoritmo][indexRate]
							bestN = n
							bestiFuntion = function
							bestAlgorithm = algoritmo
							bestRate = tasa
						if ZaccuracyVal[indexN][indexFunction][indexAlgoritmo][indexRate] > bestAccuracy:
							bestAccuracy = ZerrorVal[indexN][indexFunction][indexAlgoritmo][indexRate]

		print("Mejor número de neuronas: " + str(bestN))
		print("Mejor grado del funcion de activación: " + str(bestiFuntion))
		print("Mejor algoritmo de entrenamiento: " + str(bestAlgorithm))
		print("Mejor tasa de aprendizaje: " + str(bestRate))
		print("Mejor accuracy: " + str(bestAccuracy))
		print("Mejor error en validacion: " + str(bestRate))

		# Gráfica del error según la función de activación - Con el mejor algoritmo y tasa de aprendizaje
		lossLinear = ZerrorVal[:][0][bestAlgorithm][bestRate]
		lossExpo = ZerrorVal[:][1][bestAlgorithm][bestRate]
		lossSigmoid = ZerrorVal[:][2][bestAlgorithm][bestRate]
		lossTanh = ZerrorVal[:][3][bestAlgorithm][bestRate]
		lossRelu = ZerrorVal[:][4][bestAlgorithm][bestRate]
		plt.plot(N, lossLinear, '*-')
		plt.plot(N, lossTanh, '*-')
		plt.plot(N, lossRelu, '*-')
		plt.plot(N, lossExpo, '*-')
		plt.plot(N, lossSigmoid, '*-')
		plt.title("Error de clasificación en datos de validación según la función de activación a lo largo del número de neuronas")
		plt.xlabel("Número de neuronas (N)")
		plt.ylabel("Error de clasificación")
		plt.grid(b=True)
		plt.legend(['Linear', 'Tanh', 'Relu', 'Exponencial', 'Sigmoidal'])
		plt.show()

		# Gráfica de la exactitud según la función de activación - Con el mejor algoritmo y tasa de aprendizaje
		accuracyLinear = ZaccuracyVal[:][0][bestAlgorithm][bestRate]
		accuracyExpo = ZaccuracyVal[:][1][bestAlgorithm][bestRate]
		accuracySigmoid = ZaccuracyVal[:][2][bestAlgorithm][bestRate]
		accuracyTanh = ZaccuracyVal[:][3][bestAlgorithm][bestRate]
		accuracyRelu = ZaccuracyVal[:][4][bestAlgorithm][bestRate]
		plt.plot(N, accuracyLinear, '*-')
		plt.plot(N, accuracyTanh, '*-')
		plt.plot(N, accuracyRelu, '*-')
		plt.plot(N, accuracyExpo, '*-')
		plt.plot(N, accuracySigmoid, '*-')
		plt.title("Exactitud de clasificación en datos de validación según la función de activación a lo largo del número de neuronas")
		plt.xlabel("Número de neuronas (N)")
		plt.ylabel("Porcentaje de Exactitud")
		plt.grid(b=True)
		plt.legend(['Linear', 'Tanh', 'Relu', 'Exponencial', 'Sigmoidal'])
		plt.show()

		# Gráfica del error según el algoritmo de entrenamiento utilizado - Con la mejor funcion de activación y tasa de aprendizaje
		plt.plot(N, lossSGD, '*-')
		plt.plot(N, lossAdam, '*-')
		plt.plot(N, lossAdagrad, '*-')
		plt.title("Error de clasificación en datos de validación según el algoritmo de entrenamiento a lo largo del número de neuronas")
		plt.xlabel("Número de neuronas (N)")
		plt.ylabel("Error de clasificación")
		plt.grid(b=True)
		plt.legend(['SGD', 'Adam', 'Adagrad'])
		plt.show()

		# Gráfica de la exactitud según el algoritmo de entrenamiento utilizado - Con la mejor funcion de activación y tasa de aprendizaje
		plt.plot(N, accuracySGD, '*-')
		plt.plot(N, accuracyAdam, '*-')
		plt.plot(N, accuracyAdagrad, '*-')
		plt.title("Exactitud de clasificación en datos de validación según el algoritmo de entrenamiento a lo largo del número de neuronas")
		plt.xlabel("Número de neuronas (N)")
		plt.ylabel("Porcentaje de Exactitud")
		plt.grid(b=True)
		plt.legend(['SGD', 'Adam', 'Adagrad'])
		plt.show()

		# Gráfica del error según según la tasa de aprendizaje utilizada en el algoritmo de aprendizaje - Con la mejor funcion de activación y algoritmo de entrenamiento
		plt.plot(N, loss1, '*-')
		plt.plot(N, loss2, '*-')
		plt.plot(N, loss3, '*-')
		plt.title("Error de clasificación en datos de validación según la tasa de aprendizaje a lo largo del número de neuronas")
		plt.xlabel("Número de neuronas (N)")
		plt.ylabel("Error de clasificación")
		plt.grid(b=True)
		plt.legend(['0.001', '0.01', '0.1'])
		plt.show()

		# Gráfica de la exactitud según la tasa de aprendizaje utilizada en el algoritmo de aprendizaje - Con la mejor funcion de activación y algoritmo de entrenamiento
		plt.plot(N, accuracy1, '*-')
		plt.plot(N, accuracy2, '*-')
		plt.plot(N, accuracy3, '*-')
		plt.title("Exactitud de clasificación en datos de validación según la tasa de aprendizaje a lo largo del número de neuronas")
		plt.xlabel("Número de neuronas (N)")
		plt.ylabel("Porcentaje de Exactitud")
		plt.grid(b=True)
		plt.legend(['0.001', '0.01', '0.1'])
		plt.show()

		def neural_network_multiple_layers(self):
			NEpochs = 10#
			# Se define la función de activación de la capa oculta - Encontrada anteriormente
			function = ''#
			# Se define el algoritmo de entrenamiento de la capa oculta - Encontrado anteriormente
			algoritmo = ''
			# Se define la tasa de aprendizaje ideal para el algortimo de entrenamiento - Encontrada anteriomente
			rate = 0.01
			# Se define el número de neuronas para cada una de las capas escondidas que tiene la red neuronal - Encontrada anteriormente
			neuronas = 277
			# Se crea el vector que contiene el número de capas ocultas sobre el cuál se iterará   
			# 2, 3, 4, 5 y 6 capas ocultas son las que se van a manejar
			layers = [2, 3, 4, 5, 6]
			# Se crean los vectores de error y exactitud para definir el número de capas ocultas
			accuracyMultipleLayers = []
			lossMultipleLayers = []
			bestError = 0.5
			bestAccuracy = 0.5
			bestLayers = 2
			# Se realiza un recorrido para definir el número de capas ocultas según el número de neuronas - Encontrada anteriormente
			# Función de activación = 
			# Número de épocas = 
			# Algoritmo de entrenamiento = 
			# Tasa de aprendizaje = 
			# Número de neuronas por capa = 
			adagrad = keras.optimizers.Adagrad(rate)
			for capa in layers:
				model = keras.Sequential()
				model.add(keras.layers.Dense(30, input_dim = 30))
				for i in range(0,capa):
					model.add(keras.layers.Dense(neuronas, activation=function))
				model.add(keras.layers.Dense(1, activation="linear"))
				model.compile(loss= 'mean_squared_error', optimizer=adagrad, metrics=['accuracy'])
				model.fit(X_train, Y_train, batch_size = 10,epochs=NEpochs, verbose=0)
				loss, accuracy = model.evaluate(X_val, Y_val)
				accuracyMultipleLayers.append(accuracy)
				lossMultipleLayers.append(loss)
				if loss < bestError:
					bestError = loss
					bestLayers = capa
				if accuracy > bestAccuracy:
					bestAccuracy = accuracy
			print("Multicapa:")
			print("Menor error presentado: " + str(bestError))
			print("Mayor accuracy presentada: "+str(bestAccuracy))
			print("Número de capas de menor error: " + str(bestLayers))

			# Gráfica del error según según el número de capas ocultas usadas en la red
			plt.plot(layers, lossMultipleLayers, '*-')
			plt.title("Error de clasificación en datos de validación a lo largo del número de capas escondidas")
			plt.xlabel("Número de capas ocultas (Layers)")
			plt.ylabel("Error de clasificación")
			plt.grid(b=True)
			plt.show()

			# Gráfica de la exactitud según el número de capas ocultas usadas en la red
			plt.plot(layers, accuracyMultipleLayers, '*-')
			plt.title("Exactitud de clasificación en datos de validación a lo largo del número capas ocultas en la ")
			plt.xlabel("Número de capas ocultas (Layers)")
			plt.ylabel("Porcentaje de Exactitud")
			plt.grid(b=True)
			plt.show()


if __name__=='__main__':
    classiferWithFeatures = ClassifiersWithFeatures()
    classiferWithFeatures.main()
		
