import pyedflib as pyedf
import numpy as np
import matplotlib.pyplot as ptl
import os
import pandas as pd
import glob
import csv
import gc
import warnings
import math
import random
import tensorflow as tf
from sklearn.datasets import make_classification
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import train_test_split
from collections import Counter
from pathlib import Path
import gc
from tensorflow import keras
from tensorflow import metrics
from keras import backend as K
from keras.models import Sequential
from keras.optimizers import RMSprop
from keras.callbacks import ModelCheckpoint, Callback, EarlyStopping
from keras.optimizers import Adam
from keras.utils import np_utils
from keras.models import Sequential
from keras.layers import Input, Dropout, Flatten, Conv2D, MaxPooling2D, Dense, Activation
from keras.optimizers import RMSprop
from keras.callbacks import ModelCheckpoint, Callback, EarlyStopping
from keras.utils import np_utils
from keras.preprocessing.image import ImageDataGenerator, array_to_img, img_to_array, load_img
from numpy.fft import *


class SleepClassifier():
    def __init__(self):
        self.clear_cuda_memory()
        config = tf.compat.v1.ConfigProto(device_count = {'GPU':1,'CPU':8})
        config.gpu_options.allow_growth = True
        config.gpu_options.per_process_gpu_memory_fraction=0.9
        sess = tf.compat.v1.Session(config=config)
        tf.compat.v1.keras.backend.set_session(sess)

        self.vgg16_model = keras.applications.vgg16.VGG16(weights ="imagenet", include_top = False, input_shape = (32,32,3))
        self.vgg16_model.layers.pop()
        self.vgg16_model.layers.pop()
        self.vgg16_model.layers.pop()
        self.vgg16_model.summary()
        self.model = Sequential()

        self.accuracy_model = np.zeros(9)
        self.mse_model = np.zeros(9)
        self.accuracy_neurons = []
        self.loss_neurons = []
        self.accuracy_layer = np.zeros(5)
        self.mse_layer = np.zeros(5)
        self.best_acc_layer = 0.5
        self.best_layer = 2

        self.Dir_train = "C:/Users/jgarc/OneDrive - Universidad de los Andes/Documentos/Universidad de los Andes/Maestría/Machine Learning/Proyecto 2/Notebooks/train/" 
        
        self.Cat = ['W.csv','1.csv','2.csv','3.csv']
        self.Dir1_val = ["D:/Machine Learning/Proyecto2/Dataset/val/*.csv"]
        self.batch_size = 64

    def clear_cuda_memory(self):
        for i in range(5):tf.compat.v1.keras.backend.clear_session()
        return True

    def pre_processing(self):
        print("Begin Pre Processing")

        W_train_final = pd.DataFrame()
        L_train_final = pd.DataFrame()
        P_train_final = pd.DataFrame()
        REM_train_final = pd.DataFrame()

        W_train = pd.read_csv(self.Dir_train + '0.csv',header=None)
        print("Lee W")
        L_train = pd.read_csv(self.Dir_train + '1.csv',header = None)
        print("Lee L")
        P_train = pd.read_csv(self.Dir_train + '2.csv',header = None)
        print("Lee REM")
        REM_train = pd.read_csv(self.Dir_train + '3.csv',header = None)

        W_train.pop(3000)
        print("pop 1")
        L_train.pop(3000)
        print("faltan 2 pops")
        P_train.pop(3000)
        print("falta 1 pop")
        REM_train.pop(3000)
        print("entra al for")

        for i in range(0,len(W_train)):
            dato = W_train.iloc[i,:-1]
            dato = dato.append(dato.sample(73))
            frame = [W_train_final, dato]
            W_train_final = pd.concat(frame)
        W_train_final.to_csv('0.csv')
        print("guarda 0")
        gc.collect()

        for i in range(0,len(L_train)):
            dato1 = L_train.iloc[i,:-1]
            dato1 = dato1.append(dato1.sample(73))
            frame1 = [L_train_final, dato1]
            L_train_final = pd.concat(frame1)
        L_train_final.to_csv('1.csv')
        print("guarda 1")
        gc.collect()

        for i in range (0,len(P_train)):
            dato2 = P_train.iloc[i,:-1]
            dato2 = dato2.append(dato2.sample(73))
            frame2 = [P_train_final, dato2]
            P_train_final = pd.concat(frame2)
        P_train_final.to_csv('2.csv')
        print("guarda 2")
        gc.collect()

        for i in range (0,len(REM_train)):
            dato3 = REM_train.iloc[i,:-1]
            dato3 = dato3.append(dato3.sample(73))
            frame3 = [REM_train_final, dato3]
            REM_train_final = pd.concat(frame3)
        REM_train_final.to_csv('3.csv')
        print("guarda 3")
        gc.collect()

        print("Finished preprocessing")
    def create_real_dataset(self):
        self.train_dataset = tf.data.Dataset.list_files(str(self.Dir_train))
        self.train_dataset = self.train_dataset.map(self.process_dataset, tf.data.experimental.AUTOTUNE)
        self.train_dataset = self.train_dataset.shuffle(buffer_size = 1024, reshuffle_each_iteration=True)
        self.train_dataset = self.train_dataset.batch(self.batch_size).repeat()
        self.train_dataset = self.train_dataset.prefetch(tf.data.experimental.AUTOTUNE)
    def process_dataset(self,file_path):
        label = tf.strings.split(tf.strings.split(file_path, os.sep)[-1],'.')[0]
        label = tf.strings.to_number(label)

        data = tf.io.read_file(file_path)
        #data = tf.reshape(data,(32,32,3))
        return label, data
        self.oneLayerTraining()

    def create_training_data (self):
        ###Training data###
        self.WL_X_train = []
        self.PREM_X_train = []
        self.Y_WL = []
        self.Y_PREM = []

        self.read_W_train = pd.read_csv(self.Dir + 'train_data_W.csv',header= None)
        self.read_L_train = pd.read_csv(self.Dir + 'train_data_L.csv',header= None)
        self.read_P_train = pd.read_csv(self.Dir + 'train_data_P.csv',header= None) 
        self.read_REM_train = pd.read_csv(self.Dir + 'train_data_REM.csv',header= None) ##modificar nombres de los cs
        print("Training data")
        ###Validation data###
        self.X_validation= []
        self.Y_val = []

        self.read_W_val = pd.read_csv(self.Dir1 + 'val_data_W.csv',header= None)
        self.read_L_val = pd.read_csv(self.Dir1 + 'val_data_L.csv',header= None)
        self.read_P_val = pd.read_csv(self.Dir1 + 'val_data_P.csv',header= None) 
        self.read_REM_val = pd.read_csv(self.Dir1 + 'val_data_REM.csv',header= None) ####modificar nombres de los cs

        print("Validation data")

    def oneLayerTraining(self):
        self.N = [2,716,1430,2144,2858,3572]  ##[0,1,2,3,4,5]
        epochs = [10,8,6,4,3,2]
        best_acc = 0.5
        best_neurons_model1 = 2
        i = 0
        for i in range(0,6):
            print("Creating new model")
            self.model = Sequential()
            for layer in self.vgg16_model.layers:
                self.model.add(layer)
            for layer in self.model.layers: #Since the model is already trained with certain weights, we dont want to change it. Let it be the same
                layer.trainable = False
            self.model.add(Dense(int(self.N[i]), activation="sigmoid"))
            self.model.add(Dense(1, activation= 'linear'))
            self.model.compile(loss = 'mean_squared_error',optimizer='adam',metrics=['accuracy'])
            self.model.summary()
            self.model.fit(x = self.train_dataset, batch_size = 1,epochs= int(epochs[i]), verbose= 1)
            mse, test_acc = self.model.evaluate(self.WL_X_val, self.WL_Y_val, verbose = 1)
            self.accuracy_neurons.append(test_acc)
            self.loss_neurons.append(mse)
            self.accuracy_model[i] = test_acc
            self.mse_model[i] = mse
            if self.accuracy_model[i] > best_acc:
                best_acc = self.accuracy_model[i]
                best_neurons_model = self.N[i]
            i = i+1
            print(i)
        print("Numero de neuronas con mejor exactitud: "+str(best_neurons_model))
        print("Exactitud: "+str(best_acc))

        self.model.fit(self.PREM_X_train, self.PREM_Y_train, batch_size = 1,epochs=int(epochs[i]), verbose= 1)
        mse, test_acc = self.model.evaluate(self.PREM_X_val, self.PREM_Y_val, verbose = 1)
        self.model.save('oneLayerModel_'+str(best_neurons_model)+'_epoch_'+str(10))

        self.multipleLayers(best_neurons_model)

    def multipleLayers(self, neuronas):
        self.L = np.linspace(2,6, num=5)
        self.L.astype(int)
        self.accuracy_layer = np.zeros(5)
        self.mse_layer = np.zeros(5)
        self.best_acc_layer = 0.5
        self.best_layer = 2
        i = 0
        for i in range(0,5):
            print("Creating new model")
            self.model = Sequential()
            for layer in self.vgg16_model.layers:
                self.model.add(layer)
            for layer in self.model.layers: #Since the model is already trained with certain weights, we dont want to change it. Let it be the same
                layer.trainable = False
            for j in range(0,int(self.L[i])):
                self.model.add(Dense(neuronas, activation="sigmoid"))
            self.model.add(Dense(1, activation= 'linear'))
            self.model.compile(loss = 'mean_squared_error',optimizer='adam',metrics=['accuracy'])
            self.model.summary()
            self.model.fit(self.WL_X_train, self.WL_Y_train, batch_size = 1,epochs=10, verbose= 1)
            self.model.save('multipleLayerModel_'+str(int(self.L[i]))+'_epoch_'+str(10))
            mse, test_acc = self.model.evaluate(self.WL_X_val, self.WL_Y_val, verbose = 1)
            self.accuracy_layer[i] = test_acc
            self.mse_layer[i] = mse
            if self.accuracy_layer[i] > self.best_acc_layer:
                self.best_acc_layer= self.accuracy_layer[i]
                self.best_layer = int(self.L[i])

        print("Numero de capas con mejor exactitud: "+str(best_layer))
        print("Exactitud: "+str(best_acc_layer))

        self.model.fit(self.PREM_X_train, self.PREM_Y_train, batch_size = 1,epochs=10, verbose= 1)
        mse, test_acc = self.model.evaluate(self.PREM_X_val, self.PREM_Y_val, verbose = 1)
        self.model.save('oneLayerModel_'+str(best_layer)+'_epoch_'+str(10))

    def graphOneLayer(self):
        # Grafica de error para los distintas funciones de activacion
        plt.plot(self.N, self.accuracy_neurons, '*-')
        plt.title('Precision en la clasificacion para distintas neuronas')
        plt.xlabel('Numero de neuronas (N)')
        plt.ylabel('Precision en la clasificacion')
        plt.grid(b=True)
        plt.show()
        
        # Grafica de error para los distintas funciones de activacion
        plt.plot(self.N, self.loss_neurons, '*-')
        plt.title('Error en la clasificacion para distintas neuronas')
        plt.xlabel('Numero de neuronas (N)')
        plt.ylabel('Error en la clasificacion')
        plt.grid(b=True)
        plt.show()

    def graphMultipleLayers(self):
        # Grafica de error para los distintas funciones de activacion
        plt.plot(self.L, self.accuracy_layers, '*-')
        plt.title('Precision en la clasificacion para distintas capas ocultas')
        plt.xlabel('Numero de capas ocultas (L)')
        plt.ylabel('Precision en la clasificacion')
        plt.grid(b=True)
        plt.show()
        
        # Grafica de error para los distintas funciones de activacion
        plt.plot(self.L, self.mse_layer, '*-')
        plt.title('Error en la clasificacion para distintas capas ocultas')
        plt.xlabel('Numero de capas ocultas (L)')
        plt.ylabel('Error en la clasificacion')
        plt.grid(b=True)
        plt.show()
    
    def main(self):
        #Import input data

        self.pre_processing()
        self.create_real_dataset()
        print("Data imported")
        #Preprocess data
    
        #print("Finished preprocessing")
        self.oneLayerTraining()
        #self.multipleLayers()
        #self.graphOneLayer()
        #self.graphMultipleLayers()

if __name__=='__main__':
    sleepClass = SleepClassifier()
    sleepClass.main()







