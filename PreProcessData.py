# # Proyecto 2 - Clasifiación multiclase de señales de sueño 

# ## Juan José García Cárdenas - Código: 201515655 y Adelaida Zuluaga - Código: 201424638

# ### Punto 1: Clasificación multiclase de señales de sueño - Preprocesamiento de los datos

# #### Importación de paquetes

import pyedflib as pyedf
import numpy as np
import matplotlib.pyplot as ptl
import os
import pandas as pd
import glob
import csv
import gc
import warnings
import math
import random
from collections import Counter
from imblearn.over_sampling import RandomOverSampler
from imblearn.under_sampling import RandomUnderSampler
from pathlib import Path
from numpy.fft import *
warnings.simplefilter("ignore")


class Data_Extraction:

    def __init__(self):
        #self.clear_cuda_memory()
        #config = tf.compat.v1.ConfigProto(device_count = {'GPU':1,'CPU':8})
        #config.gpu_options.allow_growth = True
        #config.gpu_options.per_process_gpu_memory_fraction=0.9
        #sess = tf.compat.v1.Session(config=config)
        #tf.compat.v1.keras.backend.set_session(sess)
        self.scalar = StandardScaler()
        self.windowTime = 30
        self.path = r"C:\Users\jgarc\OneDrive - Universidad de los Andes\Documentos\Universidad de los Andes\Maestría\Machine Learning\Proyecto 2\Data\sleep-edf-database-expanded-1.0.0\sleep-cassette\*Hypnogram.edf"
        self.generalPath = r"C:\Users\jgarc\OneDrive - Universidad de los Andes\Documentos\Universidad de los Andes\Maestría\Machine Learning\Proyecto 2\Data\sleep-edf-database-expanded-1.0.0\sleep-cassette"
        self.W_Stage = []
        self.y_W_Stage = []
        self.L_Stage = []
        self.y_L_Stage = []
        self.P_Stage = []
        self.y_P_Stage = []
        self.REM_Stage = []
        self.y_REM_Stage = []
        self.X_resampled_LP_Stage_Data = []
        self.X_resampled_LREM_Stage_Data = []
        self.X_resampled_WL_Stage_Data = []
        self.Y_resampled_LP = []
        self.Y_resampled_LREM = []
        self.normalized_W_Stage = []
        self.normalized_L_Stage = []
        self.normalized_P_Stage = []
        self.normalized_REM_Stage = []


    def clear_cuda_memory(self):
        for i in range(5):tf.compat.v1.keras.backend.clear_session()
        return True

    def main(self):
        # Import data
        self.import_data()
        # Balance data
        self.balanced_data()
        # Normalized data
        self.normalized_data()
        # Save tag Data
        self.save_no_processed_data()

    def import_data(self):
        for file in glob.glob(self.path):
            fileName = os.path.basename(file)                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       
            names = fileName.split('-')
            psg_name = self.generalPath+ "/" +  names[0][0:7]+ "0-PSG.edf"
            st_FileHypEdf = pyedf.EdfReader(file)
            st_FileEdf = pyedf.EdfReader(psg_name)
            v_HypTime, v_HypDur, v_Hyp = st_FileHypEdf.readAnnotations()
            s_SigNum = st_FileEdf.signals_in_file
            v_Signal_Labels = st_FileEdf.getSignalLabels()
            s_SigRef = 0
            s_NSamples = st_FileEdf.getNSamples()[0]
            s_FsHz = st_FileEdf.getSampleFrequency(s_SigRef)
            v_Sig = st_FileEdf.readSignal(s_SigRef)
            s_FirstInd = 0
            contador = 0
            for i in range(0,len(v_HypTime)):
                indice = 0
                while indice*self.windowTime < v_HypDur[i]:
                    winSize = np.round(s_FsHz * self.windowTime)
                    s_LastInd = s_FirstInd + winSize
                    vectorSig = v_Sig[int(s_FirstInd):int(s_LastInd)]
                    #if vectorSig == []:
                    #    print(file)
                    if v_Hyp[i] == 'Sleep stage 1' or v_Hyp[i] == 'Sleep stage 2':
                        self.L_Stage.append(vectorSig)
                        self.y_L_Stage.append(1)
                    elif v_Hyp[i] == 'Sleep stage W':
                        self.W_Stage.append(vectorSig)
                        self.y_W_Stage.append(0)
                    elif v_Hyp[i] == 'Sleep stage 3' or v_Hyp[i] == 'Sleep stage 4':
                        self.P_Stage.append(vectorSig)
                        self.y_P_Stage.append(2)
                    elif v_Hyp[i] == 'Sleep stage R':
                        self.REM_Stage.append(vectorSig)
                        self.y_REM_Stage.append(3)
                    s_FirstInd = s_LastInd
                    indice+=1
        self.W_Stage = np.delete(self.W_Stage,np.s_[134633:135138])
        temporal = np.zeros((len(self.W_Stage),3000))
        self.W_Stage = np.array(self.W_Stage)
        self.L_Stage = np.array(self.L_Stage)
        self.P_Stage = np.array(self.P_Stage)
        self.REM_Stage = np.array(self.REM_Stage)
        for i in range(0,len(temporal)):
            for j in range(0,len(temporal[0])):
                try:
                    temporal[i,j] = self.W_Stage[i][j]
                except:
                    print(i)
                    break
        self.W_Stage=temporal
        print("Data Imported")

    def balanced_data(self):
        self.y_W_Stage = np.array(self.y_W_Stage)
        self.y_W_Stage = np.delete(self.y_W_Stage,np.s_[134633:135138])
        self.y_L_Stage = np.array(self.y_L_Stage)
        self.y_P_Stage = np.array(self.y_P_Stage)
        self.y_REM_Stage = np.array(self.y_REM_Stage)
        # Balanceo de los datos según número de datos en clases - Ventanas de 30 segundos - Without Processing
        WL_Stage_Data = np.concatenate((self.W_Stage,self.L_Stage),axis=0)
        WL_Y = np.concatenate((self.y_W_Stage,self.y_L_Stage),axis=0)
        LP_Stage_Data = np.concatenate((self.L_Stage,self.P_Stage),axis=0)
        LP_Y = np.concatenate((self.y_L_Stage,self.y_P_Stage),axis=0)
        LREM_Stage_Data = np.concatenate((self.L_Stage,self.REM_Stage),axis=0)
        LREM_Y = np.concatenate((self.y_L_Stage,self.y_REM_Stage),axis=0)
        rosOver = RandomOverSampler(random_state=0)
        rosUnder = RandomUnderSampler(random_state=0)
        self.X_resampled_LP_Stage_Data,self.Y_resampled_LP = rosOver.fit_resample(LP_Stage_Data,LP_Y)
        self.X_resampled_LREM_Stage_Data,self.Y_resampled_LREM = rosOver.fit_resample(LREM_Stage_Data,LREM_Y)
        self.X_resampled_WL_Stage_Data,self.Y_resampled_WL = rosUnder.fit_resample(WL_Stage_Data,WL_Y)
        print(sorted(Counter(self.Y_resampled_LP).items()))
        print(sorted(Counter(self.Y_resampled_LREM).items()))
        print(sorted(Counter(self.Y_resampled_WL).items()))
        self.W_Stage = self.X_resampled_WL_Stage_Data[0:len(self.L_Stage)]
        self.L_Stage = self.X_resampled_WL_Stage_Data[len(self.L_Stage):len(self.X_resampled_WL_Stage_Data)]
        self.P_Stage = self.X_resampled_LP_Stage_Data[len(self.L_Stage):len(self.X_resampled_LP_Stage_Data)]
        self.REM_Stage = self.X_resampled_LREM_Stage_Data[len(self.L_Stage):len(self.X_resampled_LREM_Stage_Data)]
        self.y_W_Stage = self.Y_resampled_WL[0:len(self.L_Stage)]
        self.y_L_Stage = self.Y_resampled_WL[len(self.L_Stage):len(self.X_resampled_WL_Stage_Data)]
        self.y_P_Stage = self.Y_resampled_LP[len(self.L_Stage):len(self.X_resampled_LP_Stage_Data)]
        self.y_REM_Stage = self.Y_resampled_LREM[len(self.L_Stage):len(self.X_resampled_LREM_Stage_Data)]
        print("Data Balanced")

    def normalized_data(self):
        self.normalized_W_Stage = self.scalar.fit_transform(self.W_Stage,self.y_W_Stage)
        self.normalized_L_Stage = self.scalar.fit_transform(self.L_Stage,self.y_L_Stage)
        self.normalized_P_Stage = self.scalar.fit_transform(self.P_Stage,self.y_P_Stage)
        self.normalized_REM_Stage = self.scalar.fit_transform(self.REM_Stage,self.y_REM_Stage)
        print("Data Normalized")

    def save_no_processed_data(self):
        W_X_train_without_Processing, W_X_test_without_Processing, W_Y_train_without_Processing, W_Y_test_without_Processing = train_test_split(self.normalized_W_Stage, self.y_W_Stage,test_size=0.15,train_size=0.85)
        W_X_train_without_Processing, W_X_val_without_Processing, W_Y_train_without_Processing, W_Y_val_without_Processing = train_test_split(W_X_train_without_Processing,W_Y_train_without_Processing, train_size=0.823)
        L_X_train_without_Processing, L_X_test_without_Processing, L_Y_train_without_Processing, L_Y_test_without_Processing = train_test_split(self.normalized_L_Stage, self.y_L_Stage,test_size=0.15,train_size=0.85)
        L_X_train_without_Processing, L_X_val_without_Processing, L_Y_train_without_Processing, L_Y_val_without_Processing = train_test_split(L_X_train_without_Processing,L_Y_train_without_Processing, train_size=0.823)
        P_X_train_without_Processing, P_X_test_without_Processing, P_Y_train_without_Processing, P_Y_test_without_Processing = train_test_split(self.normalized_P_Stage, self.y_P_Stage,test_size=0.15,train_size=0.85)
        P_X_train_without_Processing, P_X_val_without_Processing, P_Y_train_without_Processing, P_Y_val_without_Processing = train_test_split(P_X_train_without_Processing,P_Y_train_without_Processing, train_size=0.823)
        REM_X_train_without_Processing, REM_X_test_without_Processing, REM_Y_train_without_Processing, REM_Y_test_without_Processing = train_test_split(self.normalized_REM_Stage, self.y_REM_Stage,test_size=0.15,train_size=0.85)
        REM_X_train_without_Processing, REM_X_val_without_Processing, REM_Y_train_without_Processing, REM_Y_val_without_Processing = train_test_split(REM_X_train_without_Processing,REM_Y_train_without_Processing, train_size=0.823)

        print("Data splited")

        W_X_train_without_Processing = np.matrix(W_X_train_without_Processing)
        W_X_test_without_Processing = np.matrix(W_X_test_without_Processing)
        W_X_val_without_Processing = np.matrix(W_X_val_without_Processing)
        
        L_X_train_without_Processing = np.matrix(L_X_train_without_Processing)
        L_X_test_without_Processing = np.matrix(L_X_test_without_Processing)
        L_X_val_without_Processing = np.matrix(L_X_val_without_Processing)

        P_X_train_without_Processing = np.matrix(P_X_train_without_Processing)
        P_X_test_without_Processing = np.matrix(P_X_test_without_Processing)
        P_X_val_without_Processing = np.matrix(P_X_val_without_Processing)

        REM_X_train_without_Processing = np.matrix(REM_X_train_without_Processing)
        REM_X_test_without_Processing = np.matrix(REM_X_test_without_Processing)
        REM_X_val_without_Processing = np.matrix(REM_X_val_without_Processing)

        W_Y_train_without_Processing = np.matrix(W_Y_train_without_Processing)
        W_Y_test_without_Processing = np.matrix(W_Y_test_without_Processing)
        W_Y_val_without_Processing = np.matrix(W_Y_val_without_Processing)
        
        L_Y_train_without_Processing = np.matrix(L_Y_train_without_Processing)
        L_Y_test_without_Processing = np.matrix(L_Y_test_without_Processing)
        L_Y_val_without_Processing = np.matrix(L_Y_val_without_Processing)

        P_Y_train_without_Processing = np.matrix(P_Y_train_without_Processing)
        P_Y_test_without_Processing = np.matrix(P_Y_test_without_Processing)
        P_Y_val_without_Processing = np.matrix(P_Y_val_without_Processing)

        REM_Y_train_without_Processing = np.matrix(REM_Y_train_without_Processing)
        REM_Y_test_without_Processing = np.matrix(REM_Y_test_without_Processing)
        REM_Y_val_without_Processing = np.matrix(REM_Y_val_without_Processing)


        W_trainData_without_Processing = np.concatenate((W_X_train_without_Processing,np.transpose(W_Y_train_without_Processing)),axis=1)
        W_valData_without_Processing = np.concatenate((W_X_val_without_Processing,np.transpose(W_Y_val_without_Processing)),axis=1)
        W_testData_without_Processing = np.concatenate((W_X_test_without_Processing,np.transpose(W_Y_test_without_Processing)),axis=1)

        L_trainData_without_Processing = np.concatenate((L_X_train_without_Processing,np.transpose(L_Y_train_without_Processing)),axis=1)
        L_valData_without_Processing = np.concatenate((L_X_val_without_Processing,np.transpose(L_Y_val_without_Processing)),axis=1)
        L_testData_without_Processing = np.concatenate((L_X_test_without_Processing,np.transpose(L_Y_test_without_Processing)),axis=1)

        P_trainData_without_Processing = np.concatenate((P_X_train_without_Processing,np.transpose(P_Y_train_without_Processing)),axis=1)
        P_valData_without_Processing = np.concatenate((P_X_val_without_Processing,np.transpose(P_Y_val_without_Processing)),axis=1)
        P_testData_without_Processing = np.concatenate((P_X_test_without_Processing,np.transpose(P_Y_test_without_Processing)),axis=1)

        REM_trainData_without_Processing = np.concatenate((REM_X_train_without_Processing,np.transpose(REM_Y_train_without_Processing)),axis=1)
        REM_valData_without_Processing = np.concatenate((REM_X_val_without_Processing,np.transpose(REM_Y_val_without_Processing)),axis=1)
        REM_testData_without_Processing = np.concatenate((REM_X_test_without_Processing,np.transpose(REM_Y_test_without_Processing)),axis=1)


        np.savetxt("train_data_W.csv", W_trainData_without_Processing,delimiter=",")
        np.savetxt("val_data_W.csv",W_valData_without_Processing,delimiter=",")
        np.savetxt("test_data_W.csv", W_testData_without_Processing,delimiter=",")

        np.savetxt("train_data_L.csv", L_trainData_without_Processing,delimiter=",")
        np.savetxt("val_data_L.csv",L_valData_without_Processing,delimiter=",")
        np.savetxt("test_data_L.csv", L_testData_without_Processing,delimiter=",")

        np.savetxt("train_data_P.csv", P_trainData_without_Processing,delimiter=",")
        np.savetxt("val_data_P.csv",P_valData_without_Processing,delimiter=",")
        np.savetxt("test_data_P.csv", P_testData_without_Processing,delimiter=",")

        np.savetxt("train_data_REM.csv", REM_trainData_without_Processing,delimiter=",")
        np.savetxt("val_data_REM.csv",REM_valData_without_Processing,delimiter=",")
        np.savetxt("test_data_REM.csv", REM_testData_without_Processing,delimiter=",")
        print("Data Saved")

if __name__=='__main__':
    dataExtraction = Data_Extraction()
    dataExtraction.main()
