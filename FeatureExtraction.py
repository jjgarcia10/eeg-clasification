# # Proyecto 2 - Clasifiación multiclase de señales de sueño 

# ## Juan José García Cárdenas - Código: 201515655 y Adelaida Zuluaga - Código: 201424638

# ### Punto 1: Clasificación multiclase de señales de sueño - Extracción de carácteristicas manual

# #### Importación de paquetes

import pyedflib as pyedf
import numpy as np
import matplotlib.pyplot as ptl
import os
import pandas as pd
import glob
import csv
import gc
import warnings
import math
import random
from scipy import signal
from pathlib import Path
from numpy.fft import *
from scipy.signal import kaiserord, lfilter, firwin, freqz
from pylab import figure, clf, plot, xlabel, ylabel, xlim, ylim, title, grid, axes, show
warnings.simplefilter("ignore")

class FeatureExtraction:
	
	def __init__(self):
		self.t = np.linspace(0,30,3000)
		# Diseño de filtros
		self.b1, self.a1 = signal.butter(1, [0.00001,0.02], btype='bandpass')
		self.b2, self.a2 = signal.butter(1, [0.02,0.04], btype='bandpass')
		self.b3, self.a3 = signal.butter(1, [0.04,0.065], btype='bandpass')
		self.b4, self.a4 = signal.butter(1, [0.065,0.11], btype='bandpass')
		self.b5, self.a5 = signal.butter(1, [0.11, 0.99], btype='bandpass')
		# Diseño de filtros FIR
		# Params
		nyq_rate = 100 / 2.0
		width = 1.0/nyq_rate
		ripple_db = 60
		self.N, beta = kaiserord(ripple_db, width)
		cutoff_hzDelta = np.array([0.01,4])
		cutoff_hzTheta = np.array([4,8])
		cutoff_hzAlpha = np.array([8,13])
		cutoff_hzBeta = np.array([13,30])
		cutoff_hzGamma = np.array([30,49])
		# Filters
		self.Delta = firwin(self.N, cutoff_hzDelta/nyq_rate, window=('kaiser', beta), pass_zero='bandpass')
		self.Theta = firwin(self.N, cutoff_hzTheta/nyq_rate, window=('kaiser', beta), pass_zero='bandpass')
		self.Alpha = firwin(self.N, cutoff_hzAlpha/nyq_rate, window=('kaiser', beta), pass_zero='bandpass')
		self.Beta = firwin(self.N, cutoff_hzBeta/nyq_rate, window=('kaiser', beta), pass_zero='bandpass')
		self.Gamma = firwin(self.N, cutoff_hzGamma/nyq_rate, window=('kaiser', beta), pass_zero='bandpass')
		#####################
		self.FILES_TO_READ = ['train_data_W.csv','train_data_L.csv','train_data_P.csv','train_data_REM.csv','val_data_W.csv','val_data_L.csv','val_data_P.csv','val_data_REM.csv','test_data_W.csv','test_data_L.csv','test_data_P.csv','test_data_REM.csv']
		self.labels = []
		self.dataExtracted = []
		self.windowTime = 30

	def butter_bandpass(self, lowcut, highcut, fs, order=5):
		nyq = 0.5 * fs
		low = lowcut / nyq
		high = highcut / nyq
		b, a = butter(order, [low, high], btype='band')
		return b, a


	def butter_bandpass_filter(self, data, lowcut, highcut, fs, order=5):
		b, a = butter_bandpass(lowcut, highcut, fs, order=order)
		y = lfilter(b, a, data)
		return y

	def import_Data(self, file):
		file_readed = pd.read_csv(file)
		file_readed = file_readed.to_numpy()
		x_data = np.zeros((len(file_readed),len(file_readed[0])-1))
		self.labels = []
		for i in range(0,len(file_readed)):
			self.labels.append(file_readed[i][-1])
			x_data[i] = file_readed[i][:-1]
		print("File imported :"+file)
		print(len(x_data))
		return x_data

	def feature_Extraction(self):
		for file in self.FILES_TO_READ:
			self.dataExtracted = []
			data = self.import_Data(file)
			for i in range(0,len(data)):
				senal = data[i]
				delta = lfilter(self.Delta, 1.0, senal)
				theta = lfilter(self.Theta, 1.0, senal)
				alpha = lfilter(self.Alpha, 1.0, senal)
				beta = lfilter(self.Beta, 1.0, senal)
				gamma = lfilter(self.Gamma, 1.0, senal)
				#delta = signal.filtfilt(self.b1, self.a1, senal, axis=0)
				#theta = signal.filtfilt(self.b2, self.a2, senal, axis=0)
				#alpha = signal.filtfilt(self.b3, self.a3, senal, axis=0)
				#beta = signal.filtfilt(self.b4, self.a4, senal, axis=0)
				#gamma = signal.filtfilt(self.b5, self.a5, senal, axis=0)
				delta = np.array(delta)
				theta = np.array(theta)
				alpha = np.array(alpha)
				beta = np.array(beta)
				gamma = np.array(gamma)
				numberOfSeg = self.windowTime
				f_Int = 0
				MMDDelta = 0
				MMDAlpha = 0
				MMDTheta = 0
				MMDGamma = 0
				MMDBeta = 0
				for j in range(0,int(numberOfSeg)):
					l_Int = int(f_Int+len(delta)/numberOfSeg)
					minDelta = delta[f_Int:l_Int].argmin()/1000
					maxDelta = delta[f_Int:l_Int].argmax()/1000
					dDelta = math.sqrt((max(delta[f_Int:l_Int])-min(delta[f_Int:l_Int]))**2+(minDelta-maxDelta)**2)
					minTheta = theta[f_Int:l_Int].argmin()
					maxTheta = theta[f_Int:l_Int].argmax()
					dTheta = math.sqrt((max(theta[f_Int:l_Int])-min(theta[f_Int:l_Int]))**2+(minTheta-maxTheta)**2)
					minAlpha = alpha[f_Int:l_Int].argmin()
					maxAlpha = alpha[f_Int:l_Int].argmax()
					dAlpha = math.sqrt((max(alpha[f_Int:l_Int])-min(alpha[f_Int:l_Int]))**2+(minAlpha-maxAlpha)**2)
					minBeta = beta[f_Int:l_Int].argmin()
					maxBeta = beta[f_Int:l_Int].argmax()
					dBeta = math.sqrt((max(beta[f_Int:l_Int])-min(beta[f_Int:l_Int]))**2+(minBeta-maxBeta)**2)
					minGamma = gamma[f_Int:l_Int].argmin()
					maxGamma = gamma[f_Int:l_Int].argmax()
					dGamma = math.sqrt((max(gamma[f_Int:l_Int])-min(gamma[f_Int:l_Int]))**2+(minGamma-maxGamma)**2)
					MMDDelta = MMDDelta+dDelta
					MMDTheta = MMDTheta+dTheta
					MMDAlpha = MMDAlpha+dAlpha
					MMDBeta = MMDBeta+dBeta
					MMDGamma = MMDGamma+dGamma
					f_Int = l_Int
				# ESIS
				waveLength = 100
				ESIS_Delta = 0
				ESIS_Gamma = 0
				ESIS_Alpha = 0
				ESIS_Theta = 0
				ESIS_Beta = 0
				for k in range(0,len(delta)):
					f_Middle_Delta = 2
					f_Middle_Gamma = 85
					f_Middle_Alpha = 10.5
					f_Middle_Theta = 6
					f_Middle_Beta = 17.5
					vDelta = f_Middle_Delta*waveLength
					vGamma = f_Middle_Gamma*waveLength
					vAlpha = f_Middle_Alpha*waveLength
					vTheta = f_Middle_Theta*waveLength
					vBeta = f_Middle_Beta*waveLength
					ESIS_Delta = ESIS_Delta+delta[k]**2*vDelta
					ESIS_Gamma = ESIS_Gamma+gamma[k]**2*vGamma
					ESIS_Alpha = ESIS_Alpha+alpha[k]**2*vAlpha
					ESIS_Theta = ESIS_Theta+theta[k]**2*vTheta
					ESIS_Beta = ESIS_Beta+beta[k]**2*vBeta
				processed_Data = np.array([MMDDelta,MMDTheta,MMDAlpha,MMDBeta,MMDGamma,ESIS_Delta,ESIS_Gamma,ESIS_Alpha,ESIS_Theta,ESIS_Beta,self.labels[i]])
				self.dataExtracted.append(processed_Data)
			self.dataExtracted = np.array(self.dataExtracted)
			print(self.dataExtracted)
			np.savetxt("Features_"+file, self.dataExtracted,delimiter=",")
			print("Data saved for file: "+file)

	def example_Features_FIR(self, file):
		nyq_rate = 100 / 2.0
		data = self.import_Data(file)
		senal = data[0]
		delta = lfilter(self.Delta, 1.0, senal)
		theta = lfilter(self.Theta, 1.0, senal)
		alpha = lfilter(self.Alpha, 1.0, senal)
		beta = lfilter(self.Beta, 1.0, senal)
		gamma = lfilter(self.Gamma, 1.0, senal)
		t = np.arange(3000) / 100
		#------------------------------------------------
		# Plot the FIR filter coefficients.
		#------------------------------------------------

		figure(1)
		plot(self.Delta, 'bo-', linewidth=2)
		title('Filter Coefficients (%d taps)' % self.N)
		grid(True)

		figure(2)
		clf()
		w, h = freqz(self.Delta, worN=8000)
		plot((w/np.pi)*nyq_rate, np.absolute(h), linewidth=2)
		xlabel('Frequency (Hz)')
		ylabel('Gain')
		title('Frequency Response')
		ylim(-0.05, 1.05)
		grid(True)

		# Upper inset plot.
		ax1 = axes([0.42, 0.6, .45, .25])
		plot((w/np.pi)*nyq_rate, np.absolute(h), linewidth=2)
		grid(True)

		# Lower inset plot
		ax2 = axes([0.42, 0.25, .45, .25])
		plot((w/np.pi)*nyq_rate, np.absolute(h), linewidth=2)
		grid(True)

		#------------------------------------------------
		# Plot the original and filtered signals.
		#------------------------------------------------

		# The phase delay of the filtered signal.
		delay = 0.5 * (self.N-1) / 100

		figure(3)
		# Plot the original signal.
		plot(t, senal)
		# Plot the filtered signal, shifted to compensate for the phase delay.
		plot(t-delay, delta, 'r-')
		# Plot just the "good" part of the filtered signal.  The first N-1
		# samples are "corrupted" by the initial conditions.
		#plot(t[self.N-1:]-delay, delta[self.N-1:], 'g', linewidth=4)

		xlabel('t')
		grid(True)

		show()

	def example_Features_BW(self, file):
		data = self.import_Data(file)
		senal = data[0]
		delta = signal.filtfilt(self.b1, self.a1, senal)
		theta = signal.filtfilt(self.b2, self.a2, senal)
		alpha = signal.filtfilt(self.b3, self.a3, senal)
		beta = signal.filtfilt(self.b4, self.a4, senal)
		gamma = signal.filtfilt(self.b5, self.a5, senal)
		ptl.figure()
		#ptl.plot(self.t, X_train_without_Processing[1], 'b', alpha=0.75)
		ptl.plot(self.t, delta, 'r', self.t, alpha, 'k', self.t, beta, 'm')#, self.t, gamma, 'g', self.t, theta, 'c',)
		ptl.legend(('noisy signal', 'filtered'), loc='best')
		ptl.show()
		x = fft(X_train_without_Processing[1])
		xFiltDelta = fft(delta)
		xFiltTheta = fft(theta)
		xFiltAlpha = fft(alpha)
		xFiltBeta = fft(beta)
		xFiltGamma = fft(gamma)
		ptl.figure()
		#ptl.plot(t, x, 'b', alpha=0.75)
		ptl.plot(self.t, xFiltDelta, 'r', self.t, xFiltTheta, 'c', self.t, xFiltAlpha, 'k')#, self.t, xFiltBeta, 'm', self.t, xFiltGamma, 'g')
		ptl.legend(('noisy signal', 'filtered'), loc='best')
		ptl.show()

	def main(self):
		#self.feature_Extraction()
		# Ejemplos de graficacion
		#self.example_Features_FIR('val_data_W.csv')
		self.example_Features_FIR('test_data_REM.csv')


if __name__=='__main__':
    featureExtraction = FeatureExtraction()
    featureExtraction.main()


